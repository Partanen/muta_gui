# MUTA GUI
An immediate mode, source-only GUI library in C. Used in the MUTA MMORPG,
but intended to be engine neutral.

Under development and unstable, many features missing or incomplete. Development
continues along with MUTA. Might add examples some time in the future, maybe.
